

	<footer>
				<section class="footer-row" id="contact">
					<div class="footer-copy">
						<h4><?php the_field('contact_header', 'option'); ?></h4>
						<p><?php the_field('contact_copy', 'option'); ?></p>
					</div>
					<div class="footer-form">
						<?php Ninja_Forms()->display( 2 ); ?>
					</div>
				</section>
				<div class="copyright">
					<div class="signoff">
						<?php the_field('copyright', 'option'); ?>
					</div>
				</div>
			</footer>

		</div>
		
		<?php wp_footer(); ?>
		<script>
		document.getElementById('links').onclick = function (event) {
			event = event || window.event;
			var target = event.target || event.srcElement,
				link = target.src ? target.parentNode : target,
				options = {index: link, event: event},
				links = this.getElementsByTagName('a');
			blueimp.Gallery(links, options);
		};
		</script>

		<script>
			AOS.init({
				easing: 'ease-out-back',
				duration: 1000
			});
		</script>

		   <script>
	    var $root = $('html, body');
	    $('a').click(function() {
	        $root.animate({
	            scrollTop: $( $.attr(this, 'href') ).offset().top - 75
	        }, 500);
	        return false;
	    });    
    </script>

            <!-- SVG Sprite -->      
			<script>
            (function (doc) {
              var scripts = doc.getElementsByTagName('script')
              var script = scripts[scripts.length - 1]
              var xhr = new XMLHttpRequest()
              xhr.onload = function () {
                var div = doc.createElement('div')
                div.innerHTML = this.responseText
                div.style.display = 'none'
                script.parentNode.insertBefore(div, script)
              }
              xhr.open('get', '<?php bloginfo('template_url'); ?>/svg/svg-defs.svg', true)
              xhr.send()
            })(document)
          </script>             

		<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
		<script>
			window.ga = function () {
				ga.q.push(arguments)
			};
			ga.q = [];
			ga.l = +new Date;
			ga('create', 'UA-XXXXX-Y', 'auto');
			ga('send', 'pageview')
		</script>
		<script src="https://www.google-analytics.com/analytics.js" async defer></script>

	</body>
</html>
