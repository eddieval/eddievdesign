$(document).ready( function(){
  
//  $('body').bind('touchstart', function() {}); // Enable sweet hovers on touch devices and prevent sticky hovers
  
  
  // On click of menu toggle fire our functions
  $('.menu-toggle').on('click', function(){
    
    toggleMenu( $(this).find('.hamburger'), '.nav-items' );
    
  });
  
  var windowWidth = $(window).width(); // iOS resize bug fix
  
  // Reset our menu on window resize
  $(window).resize( function(){
    
    if ($(window).width() != windowWidth) {
    
      $('.hamburger').removeClass('is-active');
      $('.nav-items').removeClass('is-active');
      
      windowWidth = $(window).width();
      
    }
    
  });
  
  // Toggle our menu
  var toggleMenu = function(icon, menu) {
    
    // Menu icon
    $(icon).toggleClass('is-active');
    
    // Navigation
    $(menu).toggleClass('is-active');
    
  }
  
} );