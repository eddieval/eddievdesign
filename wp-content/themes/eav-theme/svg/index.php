<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Eddie V. Design & Code</title>

    <!-- SCD Styles -->
    <link href="css/main.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/ekko-lightbox.min.css">
    <link href='https://fonts.googleapis.com/css?family=Rubik:400,500,700' rel='stylesheet' type='text/css'>
    <link href="webfonts/ss-social.css" rel="stylesheet" />
  </head>

  <body>



    <!-- NAVBAR
    ================================================== -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://www.singlechairdesign.com">
       	   <svg class="icon-navbrand"><use xlink:href="#shape-icon_nav_small" /></use></svg>
          	<!-- <img src="img/logo_navbar.png" width="62" height="62"> -->
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
              <!-- <li><a href="#services">Services</a></li> -->
              <li><a href="#myprojects">Latest Projects</a></li>
              <li><a href="#aboutscd">About</a></li>
              <li><a class="contactme" href="#contactme">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>    


	<!-- Section #2 / Background Only -->
	<section id="second" class="story" data-speed="4" data-type="background">
		<div class="container">
			<div class="row">
				<div class="logo">
					<svg class="icon-hero"><use xlink:href="#shape-icon_hero_large" /></use></svg>
				</div>
			</div>
		</div>
	</section>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
    <div id="services"></div>
    <div class="container marketing"> 
      <div class="row">
      	<div class="container">
        <h1>A San Diego based design studio focused on great user experience and attractive design.</h1>
<!--         <div class="col-sm-6">
            <p>Vivamus fermentum semper porta. Nunc diam velit, adipiscing ut tristique vitae, sagittis vel odio. Maecenas convallis ullamcorper ultricies. Curabitur ornare, ligula semper consectetur sagittis, nisi diam iaculis velit, id fringilla sem nunc vel mi. Nam dictum, odio nec pretium volutpat, arcu ante placerat erat, non tristique elit urna et turpis. Quisque mi metus, ornare sit amet fermentum et, tincidunt et orci. Fusce eget orci a orci congue vestibulum. Ut dolor diam, elementum et vestibulum eu, porttitor vel elit. Curabitur venenatis pulvinar tellus gravida ornare.</p>
		</div>
		<div class="col-sm-6">
            <p>Sed et erat faucibus nunc euismod ultricies ut id justo. Nullam cursus suscipit nisi, et ultrices justo sodales nec. Fusce venenatis facilisis lectus ac semper. Aliquam at massa ipsum. Quisque bibendum purus convallis nulla ultrices ultricies. Nullam aliquam, mi eu aliquam tincidunt, purus velit laoreet tortor, viverra pretium nisi quam vitae mi. Fusce vel volutpat elit. Nam sagittis nisi dui.</p>          
        </div> --><!-- /.span4 -->
      	</div><!-- /container-->
      </div><!-- /.row -->
      <!-- END SERVICES -->
    </div><!-- /.container -->

	      <!-- LATEST WORK -->
	  <div id="myprojects"></div>       
      <div class="row latest">      
	      <div class="container">	      	
	      	<div class="col-sm-12"><h2>latest projects</h2></div>
		      <div class="col-sm-4">
			      <div class="project">
			      	<div class="project-thumb"><a href="img/LTN_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Living Tennessee"><img src="img/LTN_thumb.jpg" class="img-responsive" alt="Living Tennessee" width="350" height="378"></a></div>
			      	<div class="project-name">
			      		<h3>Living Tennessee</h3>
			      		<p>Custom Design &amp; Wordpress Development</p>
			      	</div>
			      </div>
		      </div>	      	
		      <div class="col-sm-4">
			      <div class="project">
			      	<div class="project-thumb"><a href="img/CR_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Dudek Cultural Resources"><img src="img/DUD_thumb.jpg" class="img-responsive" alt="Dudek Cultural Resources" width="350" height="378"></a></div>
			      	<div class="project-name">
			      		<h3>Dudek Cultural Resources Website</h3>
			      		<p>Custom Design &amp; Wordpress Development</p>
			      	</div>
			      </div>
		      </div>
		      <div class="col-sm-4">
			      <div class="project">
			      	<div class="project-thumb"><a href="img/ET_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Evans Tire"><img src="img/ET_thumb.jpg" class="img-responsive" alt="Evans Tire" width="350" height="378"></a></div>
			      	<div class="project-name">
			      		<h3>Evans Tire &amp Service Center</h3>
			      		<p>Custom Design &amp; Wordpress Development</p>
			      	</div>
			      </div>
		      </div>		      	      	      		      
	      </div>	      
      </div>
	      <!-- END LATEST WORK -->
	  
	      <!-- THE REST -->	  
	  <div class="row therest">
	  		<div class="container">
	  			<div class="row">
	  				<div class="col-sm-12"><h2>... and the best of the rest</h2></div>
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/IT_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Infusion Tech"><img src="img/samples/IT_sm.jpg" class="img-responsive" alt="Infusion Tech" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Infusion Tech</h3>
		  			    		<p>Website Design</p>
		  			    	</div>
		  			    </div>
		  			</div>	
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/PC_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Park Central Hotel"><img src="img/samples/PC_sm.jpg" class="img-responsive" alt="Park Central Hotel" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Park Central Hotel</h3>
		  			    		<p>Website Design</p>
		  			    	</div>
		  			    </div>
		  			</div>		  			

		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/HB_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Hotel Blake"><img src="img/samples/HB_sm.jpg" class="img-responsive" alt="Hotel Blake Chicago" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Hotel Blake Chicago</h3>
		  			    		<p>Website Design</p>
		  			    	</div>
		  			    </div>
		  			</div>  			
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/MTS_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Manhattan Times Square"><img src="img/samples/MTS_sm.jpg" class="img-responsive" alt="Manhattan Times Square" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Manhattan Times Square</h3>
		  			    		<p>Website Design</p>
		  			    	</div>
		  			    </div>
		  			</div>  				
	  		    </div>

	  			<div class="row">
					<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/VOP_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Villas of People"><img src="img/samples/VOP-site_sm.jpg" class="img-responsive" alt="Villas of People" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Villas of People</h3>
		  			    		<p>Design &amp; Development</p>
		  			    	</div>
		  			    </div>
		  			</div>		  				
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/PT_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Precision Temperature"><img src="img/samples/PT_sm.jpg" class="img-responsive" alt="Precision Temperature" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Precision Temperature</h3>
		  			    		<p>Design &amp; Development</p>
		  			    	</div>
		  			    </div>
		  			</div>
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/ABL_Large.jpg" target="_blank" data-toggle="lightbox" data-title="All Business Loans Infographic"><img src="img/samples/ABL_sm.jpg" class="img-responsive" alt="All Business Loans" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>All Business Loans</h3>
		  			    		<p>Infographic Design</p>
		  			    	</div>
		  			    </div>
		  			</div>			  			
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><a href="img/MM_Large.jpg" target="_blank" data-toggle="lightbox" data-title="Mancuso Media Infographic"><img src="img/samples/MM_sm.jpg" class="img-responsive" alt="Mancuso Media" width="262" height="272"></a></div>
		  			    	<div class="project-name">
		  			    		<h3>Mancuso Media, LLC</h3>
		  			    		<p>Infographic Design</p>
		  			    	</div>
		  			    </div>
		  			</div>			  			  				
	  		    </div>	  		    
	  		    
				<div class="row">
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><img src="img/samples/VOP_sm.jpg" class="img-responsive" alt="Villas of People" width="262" height="163"></div>
		  			    	<div class="project-name">
		  			    		<h3>Villas of People</h3>
		  			    		<p>Branding &amp; Logo Design</p>
		  			    	</div>
		  			    </div>
		  			</div>
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><img src="img/samples/UP_sm.jpg" class="img-responsive" alt="United Partners" width="262" height="163"></div>
		  			    	<div class="project-name">
		  			    		<h3>United Partners</h3>
		  			    		<p>Branding &amp; Logo Design</p>
		  			    	</div>
		  			    </div>
		  			</div>
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><img src="img/samples/SAC_sm.jpg" class="img-responsive" alt="Sedona Athletic Club" width="262" height="163"></div>
		  			    	<div class="project-name">
		  			    		<h3>Sedona Athletic Club</h3>
		  			    		<p>Branding &amp; Logo Design</p>
		  			    	</div>
		  			    </div>
		  			</div>
		  			<div class="col-sm-3">
		  			    <div class="project">
		  			    	<div class="project-thumb"><img src="img/samples/level2_logo_sm.jpg" class="img-responsive" alt="Level 2 Automation" width="262" height="163"></div>
		  			    	<div class="project-name">
		  			    		<h3>Level 2 Automation</h3>
		  			    		<p>Branding &amp; Logo Design</p>
		  			    	</div>
		  			    </div>
		  			</div>		
	  		    </div>
	  		</div>
	  </div>
	  	<!-- END THE REST -->
	  
	      <!-- ABOUT --> 
	  <div id="aboutscd"></div>          
      <div class="row about">      
	      <div class="container">
		      <div class="row">
		      	<div class="col-sm-12"><h2>about</h2></div>	
			      <div class="col-sm-12">
				      <p>Single-chair barbershops of old would pride themselves on serving one customer at a time and providing a one on one experience. As a small independent design studio, I pride myself on discovering the right experience for your users with a focus on results, giving you the sense of being the only customer in our shop.</p>
			      </div>				      
		      </div>		      
	      </div>
      </div>
	      <!-- END ABOUT -->
	      
	      <!-- END FOOTER -->	
	  <footer>
		  <div id="contactme"></div>      
		  <div class="row theend">
		  	<div class="container">
		  		  <div class="col-xs-12"><h2>contact</h2></div>
			      <div class="col-xs-12">
				      <p>As a small studio, I try and focus on working with clients who run small businesses, individuals, and start-ups. I also don't mind acting as a subcontractor for larger firms. If you're looking for your first website, or are ready to revamp or redesign an older one, I'm waiting to hear from you.</p>
			      </div>	  		
			</div>
	  </div> 

	  <div class="row endborder">
	  	<p>Copyright &copy; Eddie Valenzuela, 2018.</p>
	  </div>
	  	  </footer>	 
	  	<!-- END FOOTER-->	  		      	        


    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ekko-lightbox.min.js"></script>

    
    <script>
	    var $root = $('html, body');
	    $('a').click(function() {
	        $root.animate({
	            scrollTop: $( $.attr(this, 'href') ).offset().top - 75
	        }, 500);
	        return false;
	    });    
    </script>
  
    
        <!-- SVG Sprite -->      
          <script>
            (function (doc) {
              var scripts = doc.getElementsByTagName('script')
              var script = scripts[scripts.length - 1]
              var xhr = new XMLHttpRequest()
              xhr.onload = function () {
                var div = doc.createElement('div')
                div.innerHTML = this.responseText
                div.style.display = 'none'
                script.parentNode.insertBefore(div, script)
              }
              xhr.open('get', 'svg/svg-defs.svg', true)
              xhr.send()
            })(document)
          </script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2299496-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	    event.preventDefault();
	    $(this).ekkoLightbox();
	});	
</script>    
     
    
  </body>
</html>