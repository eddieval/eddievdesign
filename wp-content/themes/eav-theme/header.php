<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

        <?php wp_head(); ?>
        
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/blueimp-gallery.min.css">
        <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet"> 
        
	    <script src='<?php bloginfo('template_url'); ?>/js/fastclick.min.js'></script>
        <script src="<?php bloginfo('template_url'); ?>/js/index.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/aos.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.blueimp-gallery.min.js"></script>

    		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body>

        <div class="grid-container">

            <header>
                <nav>

                    <!-- Main Navigation -->
                    <div class="main-navigation">

                        <!-- Start pre nav -->
                        <div class="pre-nav">

                            <!-- Branding -->
                            <!-- 					<a class="brand" href="#"><img src="http://placehold.it/130x40?text=Logo" alt="Alt text" /></a> -->
                            <a class="brand" href="<?php echo esc_url( home_url( '/' ) ); ?>

">Eddie V. Design &amp; Code</a>
                            <!-- End branding -->

                            <!-- Menu toggle -->
                            <button class="menu-toggle" type="button">
                                <span class="hamburger">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                            <!-- End menu toggle -->

                        </div>
                        <!-- End pre-nav -->

                        <!-- Start Navigation Items -->

                            <?php
                                wp_nav_menu( array(
                                    'menu'              => 'Header Menu',
                                    'theme_location'    => '',
                                    'depth'             => 2,
                                    'container'         => 'div',
                                    'container_class'   => 'collapse navbar-collapse',
                                    'container_id'      => 'navbar-collapse',
                                    'menu_class'        => 'nav-items',
                                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                    'walker'            => new wp_bootstrap_navwalker())
                                );
                            ?>
                    </div>
                    <!-- End main navigation -->                    
         </nav>

