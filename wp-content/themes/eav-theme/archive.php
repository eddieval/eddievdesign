<?php get_header(); ?>

	</header>
		<!-- section -->
		<section class="blog-body">

			<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
