<?php

	/*
		Template Name: Home Template
	*/

?>

<?php get_header(); ?>
            <div class="hero-row" id="second" data-speed="4" data-type="background">
                <div class="hero-copy">
                    <div class="logo">
                        <svg class="icon-hero"><use xlink:href="#shape-icon_hero_large" /></use></svg>
                    </div>
                </div>
            </div>
		</header>

<section class="intro">

    <h2></h2>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php the_content(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		</article>
		<!-- /article -->

	<?php endif; ?>

</section>

<section class="web-row">
        <article class="card-grid-web">
            <h3><?php the_field('web_row_title'); ?></h3>
            <ul class="card-list">
                <?php 
                    $temp = $wp_query; 
                    $wp_query = null; 
                    $wp_query = new WP_Query(); 
                    $wp_query->query('showposts=12&post_type=web_work'.'&paged='.$paged); 

                    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    
                    <li class="card" data-aos="zoom-in-up">
                        
                        <?php 
                            if ( has_post_thumbnail()) {
                            $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
                            echo '<a href="' . $full_image_url[0] . '" title="' . the_title_attribute('echo=0') . '" data-gallery="#blueimp-gallery-web">';the_post_thumbnail('thumbnail');
                            echo' </a>';
                            }
                        ?>
                        
                        <h5><?php the_title(); ?></h5>
                        <p><?php the_excerpt(); ?></p>
                    </li>
                    <?php endwhile; ?>

                    <?php $wp_query = null;
                        $wp_query = $temp;
                    ?>
                    <?php else : ?>
                        <h2>Not Found</h2>
                        <p>Sorry, but you are looking for something that isn't here.</p>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>      
                </ul> 
            </article>       
</section>

<section class="print-row">
    <article class="card-grid-print">
        <h3><?php the_field('print_row_title'); ?></h3>
        <ul class="card-list">
                <?php 
                    $temp = $wp_query; 
                    $wp_query = null; 
                    $wp_query = new WP_Query(); 
                    $wp_query->query('showposts=12&post_type=print_work'.'&paged='.$paged); 
            
                    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?> 
                    
                    <li class="card" data-aos="zoom-in-up">

                        <?php 
                            if ( has_post_thumbnail()) {
                            $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
                            echo '<a href="' . $full_image_url[0] . '" title="' . the_title_attribute('echo=0') . '" data-gallery="#blueimp-gallery-print">';
                            the_post_thumbnail('thumbnail');
                            echo '</a>';
                            }
                        ?>

                        <h5><?php the_title(); ?></h5>
                        <p><?php the_excerpt(); ?></p>
                    </li>
                <?php endwhile; ?>
            
                <?php $wp_query = null;
                    $wp_query = $temp;
                ?>
                <?php else : ?>
                    <h2>Not Found</h2>
                    <p>Sorry, but you are looking for something that isn't here.</p>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
        </ul>
    </article>
</section>

<section class="about-row" id="about">
    <div class="about-content">
        <h3><?php the_field('about_title'); ?></h3>
        <p><?php the_field('about_content'); ?></p>
    </div>
</section>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<?php get_footer(); ?>