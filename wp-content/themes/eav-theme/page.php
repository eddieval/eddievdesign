<?php get_header(); ?>

        <!-- <div class="hero-row">
            <div class="hero-copy">
                <h1>Hero</h1>
                <p>These cards have been laid out using grid layout. Card 1 is between column lines 1 and 3, row lines 1
                        and 2.
                </p>
            </div>
        </div> -->
    </header>

	<section class="blog-body">

			<h2><?php the_title(); ?></h2>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
